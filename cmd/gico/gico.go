// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"gitlab.com/git-compose/git-compose/internal/cmd"
)

var (
	// BuildVersion is normal version - (Major.Minor.Patch)
	BuildVersion string
)

func main() {
	cmd.Main(BuildVersion)
}
