# Compose for Git 

[Compose for Git](https://git-compose.gitlab.io/) (or gico for short) is a tool to manage a few local repositories at once, pretty much the same way as docker-compose works.

# Features

- git-compose.yaml file contains several repository definitions
- `gico up` would clone (if needed) or update local repository from remote address in git-compose.yaml
- https or ssh alternative urls are supported
- `gico config set` to configure https or ssh preference
- can specify branch to check out for each repository (default - master)
- can specify inline or file-path git hooks to import while executing `gico up`
- `gico init` to create starting git-compose.yaml based on current directory subfolders or download remote file
- `gico push` to push in all repositories
- `gico pull` to pull in all repositories
- `gico clone` to clone in all repositories
- `gico exec` to execute any git command in all repositories
- `gico branch` to output checked out branches
- zsh or bash completion with `gico completion <shell>`
- `gico help [<command>]` to output general or specific help
- `gico doc <format>` to output documentation in md, rst or man format
- `gico serve` to start GRPC server, used to integrate to IDE

# Install

## Last binary builds

Check out the latest version in https://git-compose.gitlab.io/

Or older releases in https://gitlab.com/git-compose/git-compose/-/releases

## With Golang >= 1.12

``` bash
go get -u gitlab.com/git-compose/git-compose/cmd/gico
```

## With aptitude

There's a `.deb` package available from custom repository, you can 
read more about it [here](https://gitlab.com/git-compose/git-compose-debian).

``` bash
sudo bash -c 'echo "deb [trusted=yes] https://git-compose.gitlab.io/git-compose-debian trusty main" > /etc/apt/sources.list.d/git-compose.list'
sudo apt-get update
sudo apt-get install git-compose
gico version
```

# Usage Example

Create a file `git-compose.yaml`

``` yaml
# This file is an example of how git-compose.yaml could look like

## Version of git-compose specification
version: "0.1"

## List of repositories to manage 
repos:
  # Local name of repository could be anything suitable for directory name
  # This would be used to create a subfolder
  git-compose: 
    # URL of remote repository to clone from
    remote: https://gitlab.com/git-compose/git-compose
    branch: master
```

Run 

``` bash
gico up
```

Look how the repo was cloned:
``` bash
ls 
git --git-dir git-compose/.git/ rev-parse --abbrev-ref HEAD
```

Now change `branch: master` to `branch: test-branch` and again run 

``` bash
gico up
```

Check the branch changed

``` bash
git --git-dir git-compose/.git/ rev-parse --abbrev-ref HEAD
```

# Commands

Main `gico` commands: 
 - `clone` - clone all repositories to subfolders
 - `up` - does all necessary to bring your repos up to date
 - `branch` - outputs current branches state
 - `init` - initialize default git-compose.yaml or download existing, or generate based on subfolders

See complete CLI reference in the [documentation](https://git-compose.gitlab.io/git-compose-docs/commands.html) .

# VSCode Extension

[Visual Studio Code Extension](https://marketplace.visualstudio.com/items?itemName=git-compose.git-compose) includes bundled version of `gico` tool and allow you to work with
git-compose.yaml file from your favorite IDE (assuming it's Visual Studio Code :)

# Auto Completions

# Bash

## With bash-completion

``` bash
gico completion bash > gico
chmod +x gico
sudo mv gico /usr/share/bash-completion/completions/gico
```

## Manually via `.bashrc`

Add `source <(gico completion bash)` to your `.bashrc` like this:

``` bash
echo 'source <(gico completion bash)' >> ~/.bashrc
```

# Zsh

## With oh-my-zsh

``` bash
mkdir -p $ZSH_CUSTOM/plugins/gico
gico completion zsh > $ZSH_CUSTOM/plugins/gico/gico.plugin.zsh
```

Now you need to add `gico` to the list of enabled plugins in `~/.zshrc`

# Roadmap

## Testing

- [1.0 req] need to cover all basic scenarios in autotests
- [1.0 req] calculate proper tests coverage, require 90 percent

## IDE support

- implement Intellij IDEA plugin
- add more VCS integration in VS Code extension
- full format documentation in VS Code extension with RedHat Yaml LS
- [1.0 req] performance optimization in VS Code extension (use of `gico serve`)
- automate VS Code extension publishing via CI

## Features management

- implement `features` in git-compose.yaml: feature has name, description and list of components (repositories), where it's implemented
- support feature development by managing branches
- support environment maintenance by managing feature toggles
- support feature management in IDE plugins

## Other

- investigate and improve shell completion
- [1.0 req] keep automated changelog: generate CHANGELOG.md and debian changelog
- [1.0 req] monitor usage (f.e. with VS Code telemetry), check feedback
