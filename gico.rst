CLI
===

.. role:: bash(code)
   :language: bash

gico - helps you manage several Git repositories at once

SYNOPSIS
--------
gico

**Usage**:

:bash:`gico [GLOBAL OPTIONS] command [COMMAND OPTIONS] [ARGUMENTS...]`


GLOBAL OPTIONS
^^^^^^^^^^^^^^


--help, -h	show help (default: false)
--version, -v	print the version (default: false)



COMMANDS
--------


gico up
-----------------------------------------

does all necessary to bring your repos up to date.

Updates state of local repositories based on description in **git-compose.yaml** file.

It will:

* clone repositories missing from current dirrectory
* pull repositories, which are already on correct branches
* checkout branch if git-compose.yaml specified different branch
* writes all client hooks to .git/hooks

OPTIONS
^^^^^^^
--file value, -f value	location of git-compose.yaml file or - to take input stream
--repo value, -r value	if specified - limits to only one repository
--prefer-ssh	if specified and alternatives available in 'remotes' - would choose SSH (default: false)
--prefer-https	if specified and alternatives available in 'remotes' - would choose HTTPS (default: false)

gico init
-----------------------------------------

initiate git-compose.yaml file, possibly from remote source.

Use 'url' argument to download git-compose.yaml from remote source

gico push
-----------------------------------------

Pushes all repositories.

OPTIONS
^^^^^^^
--file value, -f value	location of git-compose.yaml file or - to take input stream
--repo value, -r value	if specified - limits to only one repository
--prefer-ssh	if specified and alternatives available in 'remotes' - would choose SSH (default: false)
--prefer-https	if specified and alternatives available in 'remotes' - would choose HTTPS (default: false)

gico exec
-----------------------------------------

Executes command in all repositories.

OPTIONS
^^^^^^^
--file value, -f value	location of git-compose.yaml file or - to take input stream
--repo value, -r value	if specified - limits to only one repository
--prefer-ssh	if specified and alternatives available in 'remotes' - would choose SSH (default: false)
--prefer-https	if specified and alternatives available in 'remotes' - would choose HTTPS (default: false)

gico clone
-----------------------------------------

Clones all repositories to subfolders.

OPTIONS
^^^^^^^
--file value, -f value	location of git-compose.yaml file or - to take input stream
--repo value, -r value	if specified - limits to only one repository
--prefer-ssh	if specified and alternatives available in 'remotes' - would choose SSH (default: false)
--prefer-https	if specified and alternatives available in 'remotes' - would choose HTTPS (default: false)

gico branch
-----------------------------------------

Outputs list of all repos with current branches.


Each line of output contains name of repository and it's current branch. 

If repository is not cloned yet, it would be printed in a list following 
line "--- following repos are not cloned yet:".

OPTIONS
^^^^^^^
--file value, -f value	location of git-compose.yaml file or - to take input stream
--prefer-ssh	if specified and alternatives available in 'remotes' - would choose SSH (default: false)
--prefer-https	if specified and alternatives available in 'remotes' - would choose HTTPS (default: false)

gico config
-----------------------------------------

access user configuration.

gico config view
-----------------------------------------

outputs current configuration.

gico config set
-----------------------------------------

modifies current configuration.

OPTIONS
^^^^^^^
--prefer-remote value	set 'https' or 'ssh' to choose default preference for remote alternatives (default: "https")

gico completion
-----------------------------------------

outputs script to autocomplete for this CLI.


Pass "bash" or "zsh" argument to choose shell

gico version
-----------------------------------------

prints current version.

OPTIONS
^^^^^^^
--normal	pass to receive only normal version: Major, Minor Patch (default: false)

gico serve
-----------------------------------------

Starts server with GRPC API similar to this CLI.

OPTIONS
^^^^^^^
--listen-port value	choose port for server to listen to (default: 8080)

gico parse
-----------------------------------------

Parses git-compose.yaml into json and adds some additional info.


Currently only ouputs line numbers for each repository and metadata with 
line number of "repos:" line. 

Command is intended to use from IDE plugins.

OPTIONS
^^^^^^^
--file value, -f value	location of git-compose.yaml file or - to take input stream
--prefer-ssh	if specified and alternatives available in 'remotes' - would choose SSH (default: false)
--prefer-https	if specified and alternatives available in 'remotes' - would choose HTTPS (default: false)

gico doc
-----------------------------------------

outputs documentation in markdown ('md'), man or rst format.

OPTIONS
^^^^^^^
--help, -h	show help (default: false)

gico help
-----------------------------------------

Shows a list of commands or help for one command.


