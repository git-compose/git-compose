module gitlab.com/git-compose/git-compose

go 1.12

require (
	github.com/go-git/go-git/v5 v5.1.0
	github.com/golang/protobuf v1.4.1
	github.com/smartystreets/goconvey v1.6.4
	github.com/urfave/cli v1.22.3 // indirect
	github.com/urfave/cli/v2 v2.2.0
	golang.org/x/arch v0.0.0-20200312215426-ff8b605520f4 // indirect
	golang.org/x/crypto v0.0.0-20200311171314-f7b00557c8c4 // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	golang.org/x/tools v0.0.0-20200313205530-4303120df7d8 // indirect
	google.golang.org/grpc v1.31.1
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v2 v2.2.8
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)
