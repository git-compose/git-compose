// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cmd

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"
)

var (
	tempTestRoot, _  = ioutil.TempDir("", fmt.Sprintf("gico_tests_%d_", time.Now().Unix()))
	thisTempTestRoot = ""

	test *testing.T
)

func initTempRoot() {
	var err error
	thisTempTestRoot, err = ioutil.TempDir(tempTestRoot, "test_")
	if err != nil {
		panic(err)
	}
	err = os.Chdir(thisTempTestRoot)
	if err != nil {
		panic(err)
	}
}

type yamlGiven struct {
	yaml string
	file string
}

func givenYaml(yaml string) *yamlGiven {
	return &yamlGiven{
		yaml: yaml,
	}
}

func (yg *yamlGiven) initate(t *testing.T) *yamlGiven {
	test = t
	initTempRoot()
	return yg
}

func (yg *yamlGiven) deleteFile() {
	path := filepath.Join(thisTempTestRoot, yg.file)
	os.Remove(path)
}

func (yg *yamlGiven) assertFile(file string) error {
	yg.file = file
	path := filepath.Join(thisTempTestRoot, file)
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	content := string(data)
	if strings.TrimSpace(content) != strings.TrimSpace(yg.yaml) {
		return fmt.Errorf("File content is different, expected: %s, got: %s", yg.yaml, content)
	}
	return nil
}

func (yg *yamlGiven) inFile(file string) *yamlGiven {
	yg.file = file
	path := filepath.Join(thisTempTestRoot, file)
	err := ioutil.WriteFile(path, []byte(yg.yaml), 0644)

	log.Printf("Writing yaml to %s", path)
	// test.Logf("Writing yaml to %s", path)
	if err != nil {
		panic(err)
	}
	return yg
}

func shouldExist(folder interface{}, expected ...interface{}) string {
	path := filepath.Join(thisTempTestRoot, folder.(string))
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return fmt.Sprintf("Folder %s is missing", folder)
	}
	return ""
}

func shouldNotExist(folder interface{}, expected ...interface{}) string {
	path := filepath.Join(thisTempTestRoot, folder.(string))
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		return fmt.Sprintf("Folder %s exists, but should not", folder)
	}
	return ""
}
