#!/bin/bash
# Copyright (c) 2020 Timur Sultanaev All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.


get_completions() {
    local completion COMP_CWORD COMP_LINE COMP_POINT COMP_WORDS COMPREPLY=()

    # load bash-completion if necessary

    if [ -f "/usr/share/bash-completion/bash_completion" ] ; then
        source /usr/share/bash-completion/bash_completion
    fi
    if [ -f "/mingw64/share/git/completion/git-completion.bash" ] ; then
        source /mingw64/share/git/completion/git-completion.bash
    fi

    COMP_LINE=$*
    COMP_POINT=${#COMP_LINE}

    eval set -- "$@"

    COMP_WORDS=("$@")

    # add '' to COMP_WORDS if the last character of the command line is a space
    [[ ${COMP_LINE[@]: -1} = ' ' ]] && COMP_WORDS+=('')

    # index of the last word
    COMP_CWORD=$(( ${#COMP_WORDS[@]} - 1 ))

    # determine completion function
    completion=$(complete -p "$1" 2>/dev/null | awk '{print $(NF-1)}')

    # run _completion_loader only if necessary
    [[ -n $completion ]] || {

        # load completion
        # _completion_loader "$1"

        # detect completion
        completion=$(complete -p "$1" 2>/dev/null | awk '{print $(NF-1)}')

    }

    # ensure completion was detected
    [[ -n $completion ]] || return 1

    # execute completion function
    "$completion"

    # print completions to stdout
    printf '%s\n' "${COMPREPLY[@]}" | LC_ALL=C sort
}

get_completions "git "