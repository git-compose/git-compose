// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cmd

import (
	"log"
	"os"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/git-compose/git-compose/internal/run"
)

func TestMain(m *testing.M) {
	// initTempRoot()

	// this test will change directory
	// and to properly cleanup we keep
	// starting directory to return back to
	startingDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	code := m.Run()
	err = os.Chdir(startingDir)
	if err != nil {
		log.Printf("Failed to cleanup: %v", err)
	}
	err = os.RemoveAll(tempTestRoot)
	if err != nil {
		log.Printf("Failed to cleanup: %v", err)
	}
	os.Exit(code)
}

func BenchmarkTwoReposClone(b *testing.B) {
	for i := 0; i < b.N; i++ {
		TestTwoReposClone(nil)
	}
}

func TestTwoReposClone(t *testing.T) {

	Convey("Given git-compose.yaml with two repositories and empty root folder", t, func() {

		givenYaml(`
version: "0.1"
repos:
  git-compose: 
    remote: https://gitlab.com/git-compose/git-compose
  git-compose-vscode:
    remote: https://gitlab.com/git-compose/git-compose-vscode`).
			initate(t).inFile("git-compose.yaml")

		Convey("Run gico parse", func() {
			err := App("0.0.0").Run([]string{"gico", "parse"})
			So(err, ShouldBeNil)
			// TODO: figure out how to capture fmt.Print
		})

		Convey("Run gico up", func() {
			err := App("0.0.0").Run([]string{"gico", "up"})
			So(err, ShouldBeNil)

			Convey("Both repos should be cloned", func() {
				So("./git-compose", shouldExist)
				So("./git-compose-vscode", shouldExist)
			})
		})

	})

}

func TestInitWithDifferentBranch(t *testing.T) {

	Convey("Given git-compose.yaml with one repo, test-branch and empty root folder", t, func() {

		composeYaml := givenYaml(`
version: "0.1"
repos:
  git-compose: 
    remote: https://gitlab.com/git-compose/git-compose
    branch: test-branch`).
			initate(t).inFile("git-compose.yaml")
		Convey("Run gico up", func() {
			err := App("0.0.0").Run([]string{"gico", "up"})
			So(err, ShouldBeNil)
			Convey("Repo should be cloned and branch should be test-branch", func() {
				So("./git-compose", shouldExist)
				So(run.GetCurrentBranch("./git-compose"), ShouldEqual, "test-branch")
				composeYaml.deleteFile()

				assertFail := givenYaml(`
version: "0.1"
repos:
  git-compose: 
    remote: https://gitlab.com/git-compose/git-compose
	branch: test-branch`).assertFile("git-compose.yaml")
				if assertFail == nil {
					log.Printf("Expected to not find the file")
					t.Fail()
				}

				Convey("Run gico init and check that branch in file is test-branch", func() {
					err := App("0.0.0").Run([]string{"gico", "init"})
					So(err, ShouldBeNil)

					assertFileContent := givenYaml(`
version: "0.1"

repos:
  git-compose: 
    # URL of remote repository to clone from
    remote: https://gitlab.com/git-compose/git-compose
    # Branch is optional, default is master
    branch: test-branch`).assertFile("git-compose.yaml")
					So(assertFileContent, ShouldBeNil)

				})

			})
		})

	})

}

func TestOneRepoWithDifferentBranch(t *testing.T) {

	Convey("Given git-compose.yaml with one repo, test-branch and empty root folder", t, func() {

		givenYaml(`
version: "0.1"
repos:
  git-compose: 
    remote: https://gitlab.com/git-compose/git-compose
    branch: test-branch`).
			initate(t).inFile("git-compose.yaml")

		Convey("Run gico parse", func() {
			err := App("0.0.0").Run([]string{"gico", "parse"})
			So(err, ShouldBeNil)
			// TODO: figure out how to capture fmt.Print
		})

		Convey("Run gico up", func() {
			err := App("0.0.0").Run([]string{"gico", "up"})
			So(err, ShouldBeNil)
			Convey("Repo should be cloned and branch should be test-branch", func() {
				So("./git-compose", shouldExist)
				So(run.GetCurrentBranch("./git-compose"), ShouldEqual, "test-branch")
			})
		})

		Convey("Run gico up and then change yaml with branch: master", func() {
			err := App("0.0.0").Run([]string{"gico", "up"})
			So(err, ShouldBeNil)
			givenYaml(`
version: "0.1"
repos:
  git-compose: 
    remote: https://gitlab.com/git-compose/git-compose
    branch: master`).
				inFile("git-compose.yaml")

			Convey("Run gico up and check branch changed to master", func() {
				err := App("0.0.0").Run([]string{"gico", "up"})
				time.Sleep(2 * time.Second)
				So(err, ShouldBeNil)
				So("./git-compose", shouldExist)
				So(run.GetCurrentBranch("./git-compose"), ShouldEqual, "master")
			})
		})

	})

	Convey("Given git-compose.yaml with one repo, master and empty root folder", t, func() {

		givenYaml(`
version: "0.1"
repos:
  git-compose: 
    remote: https://gitlab.com/git-compose/git-compose
    branch: master`).initate(t).inFile("git-compose.yaml")

		Convey("Run gico up and then change yaml with branch: test-branch", func() {
			err := App("0.0.0").Run([]string{"gico", "up"})
			So(err, ShouldBeNil)
			givenYaml(`
version: "0.1"
repos:
  git-compose: 
    remote: https://gitlab.com/git-compose/git-compose
    branch: test-branch`).inFile("git-compose.yaml")

			// in master we have gico_api
			So("./git-compose/gico_api", shouldExist)

			Convey("Run gico up and check branch changed to test-branch", func() {
				err := App("0.0.0").Run([]string{"gico", "up"})
				time.Sleep(2 * time.Second)
				So(err, ShouldBeNil)
				So("./git-compose", shouldExist)
				So(run.GetCurrentBranch("./git-compose"), ShouldEqual, "test-branch")

				// but in older branch gico_api was not present
				// so need to check it
				So("./git-compose/gico_api", shouldNotExist)
			})
		})

	})

}

// TODO: test that pulls new changes (how?)
// TODO: test inline and filepath hooks
// TODO: test path
// TODO: test file location
// TODO: test remote alternatives - with prefer options and configs
// TODO: test config set
// TODO: test get branches
