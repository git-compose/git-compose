// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/urfave/cli/v2"
	"gitlab.com/git-compose/git-compose/internal/config"
	"gitlab.com/git-compose/git-compose/internal/doc"
	"gitlab.com/git-compose/git-compose/internal/file"
	"gitlab.com/git-compose/git-compose/internal/run"
	"gitlab.com/git-compose/git-compose/internal/server"
	"gitlab.com/git-compose/git-compose/internal/user"
)

func getCompose(c *cli.Context) *file.GitCompose {
	location := c.String("file")
	compose := file.Locate(location)
	if compose == nil {
		log.Printf("Unable to locate git-compose.yaml file")
		os.Exit(1)
	}

	cfg, err := config.Read()
	if err != nil {
		log.Fatalf("Failed to read config: %v", err)
	}
	prefs := cfg.GetUserPreferences()

	if c.Bool(preferSSHFlag.Name) {
		prefs.Remote = user.PreferSSH
	}
	if c.Bool(preferHTTPSFlag.Name) {
		prefs.Remote = user.PreferHTTPS
	}
	readErr := compose.Read(prefs)
	if ok := (readErr == nil && compose.Valid()); !ok {
		log.Printf("Failed to get git-compose.yaml file: %v", readErr)
		for _, fail := range compose.Fails {
			log.Printf(fail)
		}
		os.Exit(1)
	}
	return compose

}

var (
	fileFlag = &cli.StringFlag{
		Name:    "file",
		Aliases: []string{"f"},
		Value:   "",
		Usage:   "location of git-compose.yaml file or - to take input stream",
	}
	repoFlag = &cli.StringFlag{
		Name:    "repo",
		Aliases: []string{"r"},
		Value:   "",
		Usage:   "if specified - limits to only one repository",
	}
	preferHTTPSFlag = &cli.BoolFlag{
		Name:  "prefer-https",
		Value: false,
		Usage: "if specified and alternatives available in 'remotes' - would choose HTTPS",
	}
	preferSSHFlag = &cli.BoolFlag{
		Name:  "prefer-ssh",
		Value: false,
		Usage: "if specified and alternatives available in 'remotes' - would choose SSH",
	}

	preferRemoteConfigFlag = &cli.StringFlag{
		Name:  "prefer-remote",
		Value: "https",
		Usage: "set 'https' or 'ssh' to choose default preference for remote alternatives",
	}
)

func App(normalVersion string) *cli.App {
	app := &cli.App{
		EnableBashCompletion: true,
		Version:              "v" + normalVersion,
		Name:                 "gico",
		Usage:                "helps you manage several Git repositories at once",
		Commands: []*cli.Command{
			{
				Name: "up",
				Flags: []cli.Flag{
					fileFlag, repoFlag, preferSSHFlag, preferHTTPSFlag,
				},
				Usage: "does all necessary to bring your repos up to date",
				UsageText: `Updates state of local repositories based on description in **git-compose.yaml** file.

It will:

* clone repositories missing from current dirrectory
* pull repositories, which are already on correct branches
* checkout branch if git-compose.yaml specified different branch
* writes all client hooks to .git/hooks`,
				Action: func(c *cli.Context) error {
					if _, err := run.CheckGit(); err != nil {
						return fmt.Errorf("Failed to check git tool, please install proper git version")
					}
					run.Up(getCompose(c), c.String("repo"))
					return nil
				},
			},
			{
				Name:      "init",
				Usage:     "initiate git-compose.yaml file, possibly from remote source",
				UsageText: "Use 'url' argument to download git-compose.yaml from remote source",
				ArgsUsage: "[url]",
				Action: func(c *cli.Context) error {
					if _, err := run.CheckGit(); err != nil {
						return fmt.Errorf("Failed to check git tool, please install proper git version")
					}
					url := c.Args().First()
					return run.Init(url)
				},
			},
			{
				Name: "push",
				Flags: []cli.Flag{
					fileFlag, repoFlag, preferSSHFlag, preferHTTPSFlag,
				},
				Usage: "Pushes all repositories",
				Action: func(c *cli.Context) error {
					if _, err := run.CheckGit(); err != nil {
						return fmt.Errorf("Failed to check git tool, please install proper git version")
					}
					run.Push(getCompose(c), c.String("repo"))
					return nil
				},
				BashComplete: func(c *cli.Context) {
					repoName := c.String("repo")
					if repoName != "" {
						args := append([]string{"push"}, c.Args().Slice()...)
						repo := getCompose(c).Data.Repos[repoName]
						retrieveGitCompletionForBash(repo.GetPath(), args...)
					}
				},
			},
			{
				Name: "exec",
				Flags: []cli.Flag{
					fileFlag, repoFlag, preferSSHFlag, preferHTTPSFlag,
				},
				Usage: "Executes command in all repositories",
				Action: func(c *cli.Context) error {
					if _, err := run.CheckGit(); err != nil {
						return fmt.Errorf("Failed to check git tool, please install proper git version")
					}
					run.Exec(getCompose(c), c.Args().Slice(), c.String("repo"))
					return nil
				},
				BashComplete: func(c *cli.Context) {
					repoName := c.String("repo")
					if repoName != "" {
						args := c.Args().Slice()
						repo := getCompose(c).Data.Repos[repoName]
						retrieveGitCompletionForBash(repo.GetPath(), args...)
					}
				},
			},
			{
				Name: "clone",
				Flags: []cli.Flag{
					fileFlag, repoFlag, preferSSHFlag, preferHTTPSFlag,
				},
				Usage: "Clones all repositories to subfolders",
				Action: func(c *cli.Context) error {
					if _, err := run.CheckGit(); err != nil {
						return fmt.Errorf("Failed to check git tool, please install proper git version")
					}
					run.Clone(getCompose(c), c.String("repo"))
					return nil
				},
				BashComplete: func(c *cli.Context) {
					repoName := c.String("repo")
					if repoName != "" {
						args := append([]string{"clone"}, c.Args().Slice()...)
						repo := getCompose(c).Data.Repos[repoName]
						retrieveGitCompletionForBash(repo.GetPath(), args...)
					}
				},
			},
			{
				Name: "branch",
				Flags: []cli.Flag{
					fileFlag, preferSSHFlag, preferHTTPSFlag,
				},
				Usage: "Outputs list of all repos with current branches",
				UsageText: `
Each line of output contains name of repository and it's current branch. 

If repository is not cloned yet, it would be printed in a list following 
line "--- following repos are not cloned yet:".`,
				Action: func(c *cli.Context) error {
					if _, err := run.CheckGit(); err != nil {
						return fmt.Errorf("Failed to check git tool, please install proper git version")
					}
					run.OutCurrentBranches(getCompose(c))
					return nil
				},
			},
			{
				Name:  "config",
				Usage: "access user configuration",
				Subcommands: []*cli.Command{
					{
						Name:  "view",
						Usage: "outputs current configuration",
						Action: func(c *cli.Context) error {
							return config.View()
						},
					},
					{
						Name:  "set",
						Usage: "modifies current configuration",
						Flags: []cli.Flag{
							preferRemoteConfigFlag,
						},
						Action: func(c *cli.Context) error {
							cfg, err := config.Read()
							if err != nil {
								return err
							}

							preferRemote := c.String("prefer-remote")
							if strings.ToLower(preferRemote) == "https" {
								cfg.SetRemotePreference(user.PreferHTTPS)
							} else if strings.ToLower(preferRemote) == "ssh" {
								cfg.SetRemotePreference(user.PreferSSH)
							}

							return nil
						},
					},
				},
			},
			{
				Name:  "completion",
				Usage: "outputs script to autocomplete for this CLI",
				UsageText: `
Pass "bash" or "zsh" argument to choose shell`,
				Action: func(c *cli.Context) error {
					shell := c.Args().First()
					if shell == "bash" {
						fmt.Println(BashCompletion)
					} else if shell == "zsh" {
						fmt.Println(ZshCompletion)
					}
					return nil
				},
				BashComplete: func(c *cli.Context) {
					// This will complete if no args are passed
					if c.NArg() > 0 {
						return
					}
					for _, t := range []string{"zsh", "bash"} {
						fmt.Println(t)
					}
				},
			},
			{
				Name:  "version",
				Usage: "prints current version",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:  "normal",
						Value: false,
						Usage: "pass to receive only normal version: Major, Minor Patch",
					},
				},
				Action: func(c *cli.Context) error {
					if c.Bool("normal") {
						fmt.Println(normalVersion)
					} else {
						fmt.Printf("Gico version: %s\n", normalVersion)
						if gitVersion, err := run.CheckGit(); err == nil {
							fmt.Printf("Git version: %s\n", gitVersion)
						}
					}
					return nil
				},
			},
			{
				Name:      "serve",
				Usage:     "Starts server with GRPC API similar to this CLI",
				UsageText: "",
				ArgsUsage: "",
				Flags: []cli.Flag{
					&cli.IntFlag{
						Name:  "listen-port",
						Value: 8080,
						Usage: "choose port for server to listen to",
					},
				},
				Action: func(c *cli.Context) error {
					port := c.Int("listen-port")
					if _, err := run.CheckGit(); err != nil {
						return fmt.Errorf("Failed to check git tool, please install proper git version")
					}
					return server.Run(port, normalVersion)
				},
			},
			{
				Name: "parse",
				Flags: []cli.Flag{
					fileFlag, preferSSHFlag, preferHTTPSFlag,
				},
				Usage: "Parses git-compose.yaml into json and adds some additional info",
				UsageText: `
Currently only ouputs line numbers for each repository and metadata with 
line number of "repos:" line. 

Command is intended to use from IDE plugins.`,
				Action: func(c *cli.Context) error {
					if _, err := run.CheckGit(); err != nil {
						return fmt.Errorf("Failed to check git tool, please install proper git version")
					}
					return run.Parse(getCompose(c))
				},
			},
		},
	}

	app.Commands = append(app.Commands, &cli.Command{
		Name:  "doc",
		Flags: []cli.Flag{},
		Usage: "outputs documentation in markdown ('md'), man or rst format",
		Action: func(c *cli.Context) error {
			format := c.Args().First()
			if format == "md" {
				if doc, err := app.ToMarkdown(); err == nil {
					fmt.Print(doc)
				} else {
					return err
				}
			} else if format == "man" {
				if doc, err := app.ToMan(); err == nil {
					fmt.Print(doc)
				} else {
					return err
				}
			} else if format == "rst" {
				if doc, err := doc.ToRestructuredText(app); err == nil {
					fmt.Print(doc)
				} else {
					return err
				}
			}
			return nil
		},
	})

	return app

}

func Main(normalVersion string) {
	err := App(normalVersion).Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}
