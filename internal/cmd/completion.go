// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cmd

import (
	"os"
	"os/exec"
	"strings"
)

var (
	BashCompletion = `
#! /bin/bash
_cli_bash_autocomplete() {
  if [[ "${COMP_WORDS[0]}" != "source" ]]; then
    local cur opts base
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    if [[ "$cur" == "-"* ]]; then
      opts=$( ${COMP_WORDS[@]:0:$COMP_CWORD} ${cur} --generate-bash-completion )
    else
      opts=$( ${COMP_WORDS[@]:0:$COMP_CWORD} --generate-bash-completion )
    fi
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    return 0
  fi
}

complete -o bashdefault -o default -o nospace -F _cli_bash_autocomplete gico
complete -o bashdefault -o default -o nospace -F _cli_bash_autocomplete gico.exe
complete -o bashdefault -o default -o nospace -F _cli_bash_autocomplete git-compose
`
	ZshCompletion = `
_cli_zsh_autocomplete() {

  local -a opts
  local cur
  cur=${words[-1]}
  if [[ "$cur" == "-"* ]]; then
    opts=("${(@f)$(_CLI_ZSH_AUTOCOMPLETE_HACK=1 ${words[@]:0:#words[@]-1} ${cur} --generate-bash-completion)}")
  else
    opts=("${(@f)$(_CLI_ZSH_AUTOCOMPLETE_HACK=1 ${words[@]:0:#words[@]-1} --generate-bash-completion)}")
  fi

  if [[ "${opts[1]}" != "" ]]; then
    _describe 'values' opts
  else
    _files
  fi

  return
}

compdef _cli_zsh_autocomplete gico
compdef _cli_zsh_autocomplete gico.exe
compdef _cli_zsh_autocomplete git-compose	
  `

	// TODO: extract code snippets into separate files
	GetGitCompletion = `
#!/bin/bash

get_completions() {
    local completion COMP_CWORD COMP_LINE COMP_POINT COMP_WORDS COMPREPLY=()

    # load bash-completion if necessary

    if [ -f "/usr/share/bash-completion/bash_completion" ] ; then
        source /usr/share/bash-completion/bash_completion
    fi
    if [ -f "/mingw64/share/git/completion/git-completion.bash" ] ; then
        source /mingw64/share/git/completion/git-completion.bash
    fi
    if [ -f "/usr/share/bash-completion/completions/git" ] ; then
        source /usr/share/bash-completion/completions/git
    fi

    COMP_LINE=$*
    COMP_POINT=${#COMP_LINE}

    eval set -- "$@"

    COMP_WORDS=("$@")

    # add '' to COMP_WORDS if the last character of the command line is a space
    [[ ${COMP_LINE[@]: -1} = ' ' ]] && COMP_WORDS+=('')

    # index of the last word
    COMP_CWORD=$(( ${#COMP_WORDS[@]} - 1 ))

    # determine completion function
    completion=$(complete -p "$1" 2>/dev/null | awk '{print $(NF-1)}')

    # run _completion_loader only if necessary
    [[ -n $completion ]] || {

        # load completion
        # _completion_loader "$1"

        # detect completion
        completion=$(complete -p "$1" 2>/dev/null | awk '{print $(NF-1)}')

    }

    # ensure completion was detected
    [[ -n $completion ]] || return 1

    # execute completion function
    "$completion"

    # print completions to stdout
    printf '%s\n' "${COMPREPLY[@]}"  | sed 's/ *$//g' | LC_ALL=C sort
}

get_completions 'git EXECUTIONARGS'
  `
)

func retrieveGitCompletionForBash(path string, args ...string) {
	cmd := exec.Command("bash")
	joined := strings.Join(args, "") + " "
	cmd.Stdin = strings.NewReader(strings.Replace(GetGitCompletion, "EXECUTIONARGS", joined, 1))
	cmd.Stdout = os.Stdout
	cmd.Dir = path
	cmd.Run()
}
