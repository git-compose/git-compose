// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package run

import (
	"encoding/json"
	"fmt"

	"gitlab.com/git-compose/git-compose/internal/file"
)

func Parse(compose *file.GitCompose) error {
	dat, err := json.Marshal(compose.Data)
	if err != nil {
		return fmt.Errorf("Failed to marshall data: %v", err)
	}
	fmt.Println(string(dat))
	return nil
}
