// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package run

import (
	"log"
	"os/exec"

	git "github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
)

func checkoutRepo(path string, branch string) {
	repo, err := git.PlainOpen(path)
	if err != nil {
		log.Printf("Cannot get repository: %v, fallback to CLI", err)
		checkoutRepoCLI(path, branch)
		return
	}
	worktree, err := repo.Worktree()
	if err != nil {
		log.Printf("Cannot get worktree: %v, fallback to CLI", err)
		checkoutRepoCLI(path, branch)
		return
	}

	err = worktree.Checkout(&git.CheckoutOptions{
		Branch: plumbing.ReferenceName(branch),
	})
	if err != nil {
		log.Printf("Cannot checkout: %v, fallback to CLI", err)
		checkoutRepoCLI(path, branch)
		return
	}
}

func checkoutRepoCLI(path string, branch string) {
	args := []string{"checkout", branch}
	cmd := exec.Command("git", args...)
	cmd.Dir = path
	mirror(cmd)
	cmd.Run()
}
