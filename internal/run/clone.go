// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package run

import (
	"log"
	"os"
	"os/exec"

	git "github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"

	"gitlab.com/git-compose/git-compose/internal/file"
)

func Clone(compose *file.GitCompose, repoName string) {
	if repoName != "" {
		repo := compose.Data.Repos[repoName]
		log.Printf("Cloning %s from %s", repoName, repo.Remote)
		cloneRepo(repo)
		return
	}
	for name, repo := range compose.Data.Repos {
		log.Printf("Cloning %s from %s", name, repo.Remote)
		cloneRepo(repo)
	}
}

func cloneRepo(repoDef file.RepoDef) {
	repoPath := repoDef.GetPath()
	opts := &git.CloneOptions{
		URL: repoDef.Remote,
	}

	if repoDef.Branch != "" {
		opts.ReferenceName = plumbing.ReferenceName("refs/heads/" + repoDef.Branch)
	}
	_, err := git.PlainClone(repoPath, false, opts)
	if err != nil {
		log.Printf("Cannot clone repository: %v, fallback to CLI", err)
		cloneRepoCLI(repoDef)
		return
	}
}

func cloneRepoCLI(repo file.RepoDef) {
	args := []string{"clone"}

	if repo.Branch != "" {
		args = append(args, "--branch", repo.Branch)
	}

	args = append(args, repo.Remote, repo.GetPath())
	cmd := exec.Command("git", args...)
	mirror(cmd)
	err := cmd.Run()
	if err != nil { // TODO collect errors and make final report
		log.Printf("Something went wrong during clone: %v", err)
	}
}

func mirror(cmd *exec.Cmd) {
	// TODO pipe through line modification with repo name
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
}
