// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package run

import (
	"fmt"
	"log"
	"os/exec"

	git "github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"gitlab.com/git-compose/git-compose/internal/file"
)

var (
	defaultRemoteName = "origin"
)

func fetchRepo(repoDef file.RepoDef, branch string) {
	repoPath := repoDef.GetPath()
	repo, err := git.PlainOpen(repoPath)
	if err != nil {
		log.Printf("Cannot get repository: %v, fallback to CLI", err)
		fetchRepoCLI(repoPath, branch)
		return
	}

	br, err := repo.Branch(branch)
	if err != nil {
		if err == git.ErrBranchNotFound {
			log.Printf("New branch while fetching: %s", branch)

			var remoteRef = plumbing.NewRemoteReferenceName(defaultRemoteName, branch)
			var ref, _ = repo.Reference(remoteRef, true)
			var mergeRef = plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", branch))

			err = repo.CreateBranch(&config.Branch{
				Name:   branch,
				Remote: defaultRemoteName,
				Merge:  mergeRef,
			})

			var localRef = plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", branch))
			_ = repo.Storer.SetReference(plumbing.NewHashReference(localRef, ref.Hash()))

			if err != nil {
				log.Printf("Cannot create branch: %v, fallback to CLI", err)
				fetchRepoCLI(repoPath, branch)
				return
			}
			br, err = repo.Branch(branch)
			if err != nil {
				log.Printf("Cannot get created branch: %v, fallback to CLI", err)
				fetchRepoCLI(repoPath, branch)
				return
			}
		} else {
			log.Printf("Cannot get branch: %v, fallback to CLI", err)
			fetchRepoCLI(repoPath, branch)
			return
		}
	}
	err = br.Validate()
	if err != nil {
		log.Printf("Cannot validate branch: %v, fallback to CLI", err)
		fetchRepoCLI(repoPath, branch)
		return
	}

	fetchSpec := fmt.Sprintf("+refs/heads/%s:refs/remotes/%s/%[1]s", branch, defaultRemoteName)
	err = repo.Fetch(&git.FetchOptions{
		RemoteName: defaultRemoteName,
		RefSpecs: []config.RefSpec{
			config.RefSpec(
				fetchSpec,
			),
		},
	})

	if err != nil {
		if err == git.NoErrAlreadyUpToDate {
			return
		}
		log.Printf("Cannot fetch: %v, fallback to CLI", err)
		fetchRepoCLI(repoPath, branch)
		return
	}
}

func fetchRepoCLI(path string, branch string) {
	args := []string{
		"fetch",
		defaultRemoteName,
		fmt.Sprintf("%s:%s", branch, branch),
	}
	cmd := exec.Command("git", args...)
	cmd.Dir = path
	mirror(cmd)
	cmd.Run()
}

func setUpstream(path string, branch string) {
	args := []string{
		"branch",
		fmt.Sprintf(
			"--set-upstream-to=%s/%s",
			defaultRemoteName,
			branch,
		),
		branch,
	}
	cmd := exec.Command("git", args...)
	cmd.Dir = path
	mirror(cmd)
	cmd.Run()
}
