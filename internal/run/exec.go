// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package run

import (
	"log"
	"os/exec"

	"gitlab.com/git-compose/git-compose/internal/file"
)

func Exec(compose *file.GitCompose, command []string, repoName string) {
	if repoName != "" {
		repo := compose.Data.Repos[repoName]
		log.Printf("Executing %v in %s", command, repoName)
		execRepo(repo, command)
		return
	}
	for name, repo := range compose.Data.Repos {
		log.Printf("Executing %v in %s", command, name)
		execRepo(repo, command)
	}
}

func execRepo(repo file.RepoDef, command []string) {
	cmd := exec.Command("git", command...)
	mirror(cmd)
	cmd.Dir = repo.GetPath()
	err := cmd.Run()
	if err != nil { // TODO collect errors and make final report
		log.Printf("Something went wrong during exec: %v", err)
	}
}
