// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package run

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"

	git "github.com/go-git/go-git/v5"

	"gitlab.com/git-compose/git-compose/internal/file"
)

func GetCurrentBranch(repoPath string) string {
	return getCurrentBranch(repoPath)
}

func notGit(repoPath string) bool {
	gitDir := path.Join(repoPath, ".git")
	return !dirExists(gitDir)
}

func getCurrentBranch(repoPath string) string {
	repo, err := git.PlainOpen(repoPath)
	if err != nil {
		log.Printf("Cannot get repository: %v, fallback to CLI", err)
		return getCurrentBranchByCli(repoPath)
	}

	ref, err := repo.Head()
	if err != nil {
		log.Printf("Cannot get repository head: %v, fallback to CLI", err)
		return getCurrentBranchByCli(repoPath)
	}

	branches, err := repo.Branches()
	if err != nil {
		log.Printf("Cannot get repository branches: %v, fallback to CLI", err)
		return getCurrentBranchByCli(repoPath)
	}
	for {
		branch, err := branches.Next()

		if branch == nil || err != nil {
			if err != nil {
				log.Printf("Failed to get repository branch: %v, fallback to CLI", err)
			}
			break
		}
		if branch.Hash() == ref.Hash() {
			return branch.Name().Short()
		}
	}
	log.Printf("Cannot find branch of %s, fallback to CLI", repoPath)
	return getCurrentBranchByCli(repoPath)
}

func getCurrentBranchByCli(repoPath string) string {

	args := []string{"rev-parse", "--abbrev-ref", "HEAD"}
	cmd := exec.Command("git", args...)
	cmd.Dir = repoPath
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr

	var outb bytes.Buffer
	cmd.Stdout = &outb

	if e := cmd.Run(); e != nil {
		panic(e)
	}
	return strings.TrimSpace(outb.String())
}

func GetCurrentBranches(compose *file.GitCompose) map[string]string {
	branches := make(map[string]string)
	for name, repo := range compose.Data.Repos {
		repoPath := repo.GetPath()
		if notExists(repoPath) || notGit(repoPath) {
			continue
		}
		branch := getCurrentBranch(repoPath)
		branches[name] = branch
	}
	return branches
}

func OutCurrentBranches(compose *file.GitCompose) {
	var notExistent []string
	for name, repo := range compose.Data.Repos {
		repoPath := repo.GetPath()
		if notExists(repoPath) || notGit(repoPath) {
			notExistent = append(notExistent, name)
			continue
		}
		branch := getCurrentBranch(repoPath)
		fmt.Printf("%s %s\n", name, branch)
	}
	if len(notExistent) > 0 {
		fmt.Printf("--- following repos are not cloned yet:\n")
	}
	for _, name := range notExistent {
		fmt.Printf("%s\n", name)
	}
}
