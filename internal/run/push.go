// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package run

import (
	"log"
	"os/exec"

	"gitlab.com/git-compose/git-compose/internal/file"
)

func Push(compose *file.GitCompose, repoName string) {
	if repoName != "" {
		repo := compose.Data.Repos[repoName]
		log.Printf("Pushing %s to %s", repoName, repo.Remote)
		pushRepo(repo.GetPath(), repo)
		return
	}
	for name, repo := range compose.Data.Repos {
		log.Printf("Pushing %s to %s", name, repo.Remote)
		pushRepo(repo.GetPath(), repo)
	}
}

func pushRepo(path string, repo file.RepoDef) {
	args := []string{"push"}
	cmd := exec.Command("git", args...)
	mirror(cmd)
	cmd.Dir = path
	err := cmd.Run()
	if err != nil { // TODO collect errors and make final report
		log.Printf("Something went wrong during push: %v", err)
	}
}
