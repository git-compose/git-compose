// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package run

import (
	"log"
	"os"

	"gitlab.com/git-compose/git-compose/internal/file"
)

func Up(compose *file.GitCompose, repoName string) {
	if repoName != "" {
		name := repoName
		repo := compose.Data.Repos[repoName]
		path := repo.GetPath()

		log.Printf("Upping %s", name)

		if notExists(path) {
			log.Printf("%s folder does not exist, cloning", name)
			cloneRepo(repo)
			return
		}

		if fileExists(path) { // TODO collect for final report
			log.Printf("There is a file, where repo folder is expected: %s", name)
			return
		}
		up(compose, repo, name)
		return
	}

	for name, repo := range compose.Data.Repos {
		path := repo.GetPath()
		log.Printf("Upping %s", name)

		if notExists(path) {
			log.Printf("%s folder does not exist, cloning", name)
			cloneRepo(repo)
			continue
		}

		if fileExists(path) { // TODO collect for final report
			log.Printf("There is a file, where repo folder is expected: %s", name)
			continue
		}
		up(compose, repo, name)
	}
}

func up(compose *file.GitCompose, repo file.RepoDef, name string) {
	// directory exists

	path := repo.GetPath()
	current := getCurrentBranch(path)
	required := repo.Branch

	if required == "" {
		required = "master"
	}

	if current == required {
		pullRepo(repo)
	} else {
		log.Printf("Current branch %s is different from required %s, switching", current, required)
		upChangeBranch(name, required, repo)
	}

	if err := setupHooks(name, repo); err != nil {
		log.Printf("Failed to setup git hooks: %v", err)
	}

}

func upChangeBranch(name string, required string, repo file.RepoDef) {
	path := repo.GetPath()
	log.Printf("Fetching %s, branch %s", name, required)
	fetchRepo(repo, required)
	// fetchRepoCLI(path, required)
	log.Printf("Checking out %s", name)
	checkoutRepo(path, required)
	//log.Printf("Setting upstream for %s", name)
	//setUpstream(path, required)
	log.Printf("Pulling %s", name)
	pullRepo(repo)
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func dirExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

func notExists(filename string) bool {
	_, err := os.Stat(filename)
	return os.IsNotExist(err)
}
