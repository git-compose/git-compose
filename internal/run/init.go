// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package run

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"

	git "github.com/go-git/go-git/v5"
	"gitlab.com/git-compose/git-compose/internal/file"
)

func initFile() error {
	files, err := ioutil.ReadDir("./")

	repos := make([]os.FileInfo, 0)
	if err == nil && len(files) > 0 {
		for _, repoCandidate := range files {
			if repoCandidate.IsDir() {
				log.Printf("Checking directory %s for git repository", repoCandidate.Name())
				if dirExists("./" + repoCandidate.Name() + "/.git") {
					//TODO: more correct check
					repos = append(repos, repoCandidate)
				}
			}
		}
	}

	if len(repos) > 0 {
		log.Printf(
			"Located %d local repositories, building %s",
			len(repos),
			file.DefaultLocation,
		)

		type LocatedRepo struct {
			Dir    string
			Remote string
			Branch string
		}
		tmpl, err := template.New("").Parse(
			`
version: "0.1"

repos:{{range .Repos}}
  {{.Dir}}: 
    # URL of remote repository to clone from
    remote: {{.Remote}}{{if not ( eq .Branch "" )}}
    # Branch is optional, default is master
    branch: {{.Branch}}{{end}}
{{end}}`)
		if err != nil {
			return err
		}

		locatedRepos := make([]LocatedRepo, 0)
		for _, repofile := range repos {
			remote, remoteErr := getRemote(repofile.Name())
			if remoteErr != nil {
				log.Printf("Failed to determine remote for %s: %v", repofile.Name(), remoteErr)
			} else {
				repo := LocatedRepo{
					repofile.Name(),
					remote,
					GetCurrentBranch(repofile.Name()),
				}
				locatedRepos = append(locatedRepos, repo)
			}
		}
		type Model struct {
			Repos []LocatedRepo
		}
		var tpl bytes.Buffer
		err = tmpl.Execute(&tpl, Model{Repos: locatedRepos})
		if err != nil {
			return err
		}

		err = ioutil.WriteFile(file.DefaultLocation, tpl.Bytes(), 0644)
		return err

	}

	return file.InitDefault()
}

func Init(url string) error {
	if file.Exists() {
		return fmt.Errorf("Cannot initialize: this directory already have git-compose file")
	}
	if url != "" {
		return download(file.DefaultLocation, url)
	}
	return initFile()
}

func download(path string, url string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	out, err := os.Create(path)
	if err != nil {
		return err
	}
	defer out.Close()
	_, err = io.Copy(out, resp.Body)
	return err
}

func getRemote(repoDir string) (string, error) {
	repo, err := git.PlainOpen(repoDir)
	if err != nil {
		log.Printf("Cannot get repository: %v, fallback to CLI", err)
		return getRemoteCLI(repoDir)
	}
	remote, err := repo.Remote("origin")
	if err != nil {
		log.Printf("Cannot get remote: %v, fallback to CLI", err)
		return getRemoteCLI(repoDir)
	}
	urls := remote.Config().URLs
	if len(urls) < 1 {
		log.Printf("Cannot get remote: empty urls list, fallback to CLI")
		return getRemoteCLI(repoDir)
	}
	return urls[0], nil
}

func getRemoteCLI(repoDir string) (string, error) {

	args := []string{"config", "--get", "remote.origin.url"}
	cmd := exec.Command("git", args...)
	cmd.Dir = repoDir
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr

	var outb bytes.Buffer
	cmd.Stdout = &outb

	if e := cmd.Run(); e != nil {
		return "", e
	}
	return strings.TrimSpace(outb.String()), nil
}
