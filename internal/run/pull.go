// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package run

import (
	"log"
	"os/exec"

	git "github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"

	"gitlab.com/git-compose/git-compose/internal/file"
)

func Pull(compose *file.GitCompose, repoName string) {
	if repoName != "" {
		repo := compose.Data.Repos[repoName]
		log.Printf("Pulling %s to %s", repoName, repo.Remote)
		pullRepo(repo)
		return
	}
	for name, repo := range compose.Data.Repos {
		log.Printf("Pulling %s to %s", name, repo.Remote)
		pullRepo(repo)
	}
}

func pullRepoCLI(repo file.RepoDef) {
	args := []string{"pull"}
	cmd := exec.Command("git", args...)
	cmd.Dir = repo.GetPath()
	mirror(cmd)
	cmd.Run()
}

func pullRepo(repoDef file.RepoDef) {
	repoPath := repoDef.GetPath()
	repo, err := git.PlainOpen(repoPath)
	if err != nil {
		log.Printf("Cannot get repository: %v, fallback to CLI", err)
		pullRepoCLI(repoDef)
		return
	}

	worktree, err := repo.Worktree()
	if err != nil {
		log.Printf("Cannot get worktree: %v, fallback to CLI", err)
		pullRepoCLI(repoDef)
		return
	}

	opts := &git.PullOptions{
		SingleBranch: true,
	}
	if repoDef.Branch != "" {
		opts.ReferenceName = plumbing.ReferenceName("refs/heads/" + repoDef.Branch)
	}
	err = worktree.Pull(opts)

	if err != nil {
		if err == git.NoErrAlreadyUpToDate {
			return
		}
		log.Printf("Cannot pull: %v, fallback to CLI", err)
		pullRepoCLI(repoDef)
		return
	}
}
