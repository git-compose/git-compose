// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package run

import (
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/git-compose/git-compose/internal/file"
)

func setupHooks(name string, def file.RepoDef) error {
	if len(def.Hooks) > 0 {
		log.Printf("Installing %d hooks for repository %s", len(def.Hooks), name)

		for hook, hookDef := range def.Hooks {
			file := fmt.Sprintf("%s/.git/hooks/%s", name, hook)
			if hookDef.Script != "" {
				err := ioutil.WriteFile(file, []byte(hookDef.Script), 0755)
				if err != nil {
					return err
				}
			}

			if hookDef.File != "" {
				fromFile := fmt.Sprintf("%s/%s", name, hookDef.File)
				dat, err := ioutil.ReadFile(fromFile)
				if err != nil {
					return err
				}

				err = ioutil.WriteFile(file, dat, 0755)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}
