// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package run

import (
	"bytes"
	"log"
	"os"
	"os/exec"
	"strings"
)

func CheckGit() (string, error) {
	cmd := exec.Command("git", "version")
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr

	var outb bytes.Buffer
	cmd.Stdout = &outb

	if e := cmd.Run(); e != nil {
		log.Printf("Failed to check git version: %v", e)
		return "", e
	}
	return strings.TrimSpace(outb.String()), nil
}
