// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package file

import (
	"os"
)

type GitCompose struct {
	Path      string
	Fails     []string
	Data      *GitComposeFileData
	FromInput bool
}

var (
	DefaultLocation = "git-compose.yaml"
)

func Locate(input string) *GitCompose {
	if input == "-" {
		return &GitCompose{
			FromInput: true,
		}
	}
	// stat, _ := os.Stdin.Stat()
	// if (stat.Mode() & os.ModeCharDevice) == 0 {
	// 	return &GitCompose{
	// 		FromInput: true,
	// 	}
	// }

	if input == "" {
		input = DefaultLocation
	}

	if !fileExists(input) {
		return nil
	}
	return &GitCompose{
		Path: input,
	}
}

func Exists() bool {
	return fileExists(DefaultLocation)
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
