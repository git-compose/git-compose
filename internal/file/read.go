// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package file

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/git-compose/git-compose/internal/user"
	"gopkg.in/yaml.v3"
)

type GitComposeFileVersioned struct {
	Version string `yaml:"version"`
}

type HookDef struct {
	Script string `yaml:"script" json:"script"`
	File   string `yaml:"file" json:"file"`
}

type RemoteAlternatives struct {
	SSH   string `yaml:"ssh" json:"ssh"`
	HTTPS string `yaml:"https" json:"https"`
}

type RepoDef struct {
	Name       string             `yaml:"name" json:"name"`
	Path       string             `yaml:"path" json:"path"`
	Remote     string             `yaml:"remote" json:"remote"`
	Remotes    RemoteAlternatives `yaml:"remotes" json:"remotes"`
	Branch     string             `yaml:"branch" json:"branch"`
	LineNumber int                `yaml:"line" json:"line"`
	Hooks      map[string]HookDef `yaml:"hooks" json:"hooks"`
	node       yaml.Node
}

type GitComposeFileMetadata struct {
	ReposLineNumber int `yaml:"reposLineNumber" json:"reposLineNumber"`
}

type GitComposeFileData struct {
	Repos    map[string]RepoDef     `yaml:"repos" json:"repos"`
	Metadata GitComposeFileMetadata `yaml:"metadata" json:"metadata"`
}

type GitComposeFileDataV01 struct {
	Repos yaml.Node `yaml:"repos"`
}

func read(compose *GitCompose) ([]byte, error) {
	if compose.FromInput {
		reader := bufio.NewReader(os.Stdin)
		return ioutil.ReadAll(reader)
	}
	return ioutil.ReadFile(compose.Path)
}

func (compose *GitCompose) Read(prefs user.Preferences) error {
	dat, err := read(compose)
	if err != nil {
		// don't expect to panic unless file was deleted in between
		panic(err)
	}
	return compose.parse(dat, prefs)
}

func (compose *GitCompose) parse(dat []byte, prefs user.Preferences) error {

	versioned := GitComposeFileVersioned{}
	err := yaml.Unmarshal(dat, &versioned)
	if err != nil {
		return compose.fail(fmt.Sprintf("could not parse version: %v \n\n Are you sure this is a the correct git-compose.yaml file: %s?", err, compose.Path))
	}

	if versioned.Version != "0.1" {
		return compose.fail(fmt.Sprintf("Unknown version of git-compose %s in %s, can only work with 0.1, maybe you need to update your git-compose?", versioned.Version, compose.Path))
	}

	data := GitComposeFileDataV01{}
	err = yaml.Unmarshal(dat, &data)
	if err != nil {
		return compose.fail(fmt.Sprintf("could not parse %s: %v", compose.Path, err))
	}

	// repos := make(map[string]RepoDef)
	var repos map[string]*RepoDef
	data.Repos.Decode(&repos)

	for _, node := range data.Repos.Content {
		if node.ShortTag() == "!!str" {
			name := node.Value
			repo := repos[name]

			if repo != nil {
				repo.Name = name
				repo.LineNumber = node.Line
			}
		}
	}

	var reposIm = make(map[string]RepoDef)

	for name, repo := range repos {
		if repo.Remote == "" {
			if repo.Remotes.HTTPS != "" {
				repo.Remote = repo.Remotes.HTTPS
				if repo.Remotes.SSH != "" {
					if prefs.Remote == user.PreferSSH {
						repo.Remote = repo.Remotes.SSH
					}
				}
			} else if repo.Remotes.SSH != "" {
				repo.Remote = repo.Remotes.SSH
			}
		}

		reposIm[name] = *repo
	}

	compose.Data = &GitComposeFileData{
		Repos: reposIm,
	}

	var root yaml.Node
	err = yaml.Unmarshal(dat, &root)

	for _, node := range root.Content[0].Content {
		if node.ShortTag() == "!!str" && node.Value == "repos" {
			compose.Data.Metadata.ReposLineNumber = node.Line
		}
	}

	return nil
}

func (compose *GitCompose) fail(errMessage string) error {
	er := fmt.Errorf("Could not read file: %s", errMessage)
	compose.Fails = append(compose.Fails, er.Error())
	return er
}
