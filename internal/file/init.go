// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package file

import (
	"io/ioutil"
	"os"
)

func dirExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

func InitDefault() error {
	return initDefault()
}

func initDefault() error {
	initTemplate := []byte(`
version: "0.1"
repos:
# Local name of repository could be anything suitable for directory name
# This would be used to create a subfolder
# git-compose: 
  # URL of remote repository to clone from
  # remote: https://gitlab.com/git-compose/git-compose
  # Branch is optional
  # branch: master
`)
	err := ioutil.WriteFile(DefaultLocation, initTemplate, 0644)
	return err
}
