// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package file

import "fmt"

func (compose *GitCompose) Valid() bool {
	if len(compose.Data.Repos) < 1 {
		compose.fail("Compose file should have at least one repository specified")
		return false
	}

	ok := true
	for name, repo := range compose.Data.Repos {
		if repo.Remote == "" {
			compose.fail(fmt.Sprintf("Repository %s does not have remote URL specified", name))
			ok = false
		}
		for hook, hookDef := range repo.Hooks {
			if hookDef.File != "" && hookDef.Script != "" {
				compose.fail(fmt.Sprintf("Hook %s in repository %s have both file and script specified, which is not allowed", name, hook))
				ok = false
			}
		}
	}
	return ok
}
