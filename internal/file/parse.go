// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package file

import (
	"log"

	"gitlab.com/git-compose/git-compose/internal/config"
)

func Parse(data string) (*GitCompose, error) {
	compose := &GitCompose{
		Path: "",
	}
	cfg, err := config.Read()
	if err != nil {
		log.Printf("Failed to read config: %v", err)
		return nil, err
	}
	prefs := cfg.GetUserPreferences()
	err = compose.parse([]byte(data), prefs)
	if err != nil {
		log.Printf("Failed to parse gico file: %v", err)
		return nil, err
	}
	return compose, nil
}
