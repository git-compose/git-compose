// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package file

func (repo *RepoDef) GetPath() string {
	if repo.Path == "" {
		return repo.Name
	} else {
		return repo.Path
	}
}
