// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package server

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/git-compose/git-compose/gico_api"
	"gitlab.com/git-compose/git-compose/internal/config"
	"gitlab.com/git-compose/git-compose/internal/file"
	"gitlab.com/git-compose/git-compose/internal/run"
	"google.golang.org/grpc"
)

type gicoServer struct {
	normalVersion string
}

func (s *gicoServer) GetVersion(
	ctx context.Context,
	req *gico_api.ReqVersion) (*gico_api.Version, error) {
	return &gico_api.Version{
		Normal: s.normalVersion,
	}, nil
}

func (s *gicoServer) Up(
	ctx context.Context,
	req *gico_api.ReqRepo) (*gico_api.Res, error) {
	run.Up(getCompose(req.File), req.Repo)
	return &gico_api.Res{}, nil
}
func (s *gicoServer) Pull(
	ctx context.Context,
	req *gico_api.ReqRepo) (*gico_api.Res, error) {
	run.Pull(getCompose(req.File), req.Repo)
	return &gico_api.Res{}, nil
}
func (s *gicoServer) Push(
	ctx context.Context,
	req *gico_api.ReqRepo) (*gico_api.Res, error) {
	run.Push(getCompose(req.File), req.Repo)
	return &gico_api.Res{}, nil
}

func (s *gicoServer) Init(
	ctx context.Context,
	req *gico_api.ReqInit) (*gico_api.Res, error) {
	return &gico_api.Res{}, run.Init(req.Url)
}

func (s *gicoServer) ParseFile(
	ctx context.Context,
	req *gico_api.ReqFile) (*gico_api.ComposeFile, error) {
	var compose *file.GitCompose
	var err error

	if req.File != "" {
		compose = getCompose(req.File)
	}

	if req.Data != "" {
		compose, err = file.Parse(req.Data)
		if err != nil {
			return nil, err
		}
	}

	file := &gico_api.ComposeFile{
		Metadata: &gico_api.Metadata{
			ReposLineNumber: int64(compose.Data.Metadata.ReposLineNumber),
		},
	}

	file.Repos = make(map[string]*gico_api.RepoDef)
	for name, repo := range compose.Data.Repos {
		file.Repos[name] = &gico_api.RepoDef{
			Line: int64(repo.LineNumber),
			Path: repo.Path,
		}
	}
	return file, nil

}

func (s *gicoServer) GetBranches(
	ctx context.Context,
	req *gico_api.ReqBranches) (*gico_api.ResBranches, error) {
	branchesMap := run.GetCurrentBranches(getCompose(req.File))
	branches := make([]*gico_api.RepoBranch, 0)
	for repo, branch := range branchesMap {
		if repo == "" {
			continue
		}
		branches = append(branches, &gico_api.RepoBranch{
			Branch: branch,
			Repo:   repo,
		})
	}
	return &gico_api.ResBranches{
		Branches: branches,
	}, nil
}

func getCompose(location string) *file.GitCompose {
	compose := file.Locate(location)
	if compose == nil {
		log.Printf("Unable to locate git-compose.yaml file")
		return nil
	}

	cfg, err := config.Read()
	if err != nil {
		log.Printf("Failed to read config: %v", err)
		return nil
	}
	prefs := cfg.GetUserPreferences()
	readErr := compose.Read(prefs)
	if ok := (readErr == nil && compose.Valid()); !ok {
		log.Printf("Failed to get git-compose.yaml file: %v", readErr)
		for _, fail := range compose.Fails {
			log.Printf(fail)
		}
		return nil
	}
	return compose
}

func Run(port int, normalVersion string) error {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return err
	}
	grpcServer := grpc.NewServer()
	gico_api.RegisterGicoServer(grpcServer, &gicoServer{normalVersion: normalVersion})
	go func() {
		sig := <-sigs
		log.Printf("Stopping by sig %v", sig)
		grpcStopKiller := make(chan bool)

		go func() {
			select {
			case <-time.After(10 * time.Second):
				log.Printf("GRPC was not stopped gracefully after 10 seconds, killing...")
				grpcServer.Stop()
				log.Printf("GRPC has stopped forcefully after timeout")
			case <-grpcStopKiller:
				log.Printf("GRPC has stopped gracefully")
			}
		}()
		grpcServer.GracefulStop()
		grpcStopKiller <- true

	}()
	log.Printf("Starting server...")
	err = grpcServer.Serve(lis)
	if err != nil {
		log.Printf("Server has failed: %v", err)
	}

	return err
}
