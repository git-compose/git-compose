// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package user

type PreferRemote string

const(
    PreferSSH PreferRemote = "SSH"
    PreferHTTPS PreferRemote = "HTTPS"
)

type Preferences struct {
	Remote PreferRemote
}