// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package config

import (
	"strings"

	"gitlab.com/git-compose/git-compose/internal/user"
)

func (cfg *File) GetUserPreferences() user.Preferences {
	var remote user.PreferRemote
	if strings.ToLower(cfg.Preferences.Remote) == "https" {
		remote = user.PreferHTTPS
	}
	if strings.ToLower(cfg.Preferences.Remote) == "ssh" {
		remote = user.PreferSSH
	}
	if remote == "" {
		remote = user.PreferHTTPS
	}
	return user.Preferences{
		Remote: remote,
	}
}
