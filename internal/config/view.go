// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
)

func View() error {
	usr, err := user.Current()
	if err != nil {
		return err
	}

	gicoFolder := ensureFolder(usr.HomeDir)
	configPath := filepath.Join(gicoFolder, "config.yaml")

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		defaultConfig, err := initDefault(configPath)
		if err != nil {
			return err
		}
		fmt.Print(defaultConfig)
		return nil
	}

	dat, err := ioutil.ReadFile(configPath)
	if err != nil {
		return err
	}

	fmt.Print(string(dat))

	return nil

}
