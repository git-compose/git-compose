// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package config

import (
	"io/ioutil"
	osUser "os/user"
	"path/filepath"
	"strings"

	"gitlab.com/git-compose/git-compose/internal/user"
	"gopkg.in/yaml.v2"
)

func (cfg *File) SetRemotePreference(remote user.PreferRemote) error {
	cfg.Preferences.Remote = strings.ToLower(string(remote))
	data, err := yaml.Marshal(cfg)
	if err != nil {
		return err
	}
	usr, err := osUser.Current()
	if err != nil {
		return err
	}
	gicoFolder := ensureFolder(usr.HomeDir)
	configPath := filepath.Join(gicoFolder, "config.yaml")

	err = ioutil.WriteFile(configPath, data, 0644)
	if err != nil {
		return err
	}

	return nil
}
