// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package config

import (
	"io/ioutil"
	"os"
	osUser "os/user"
	"path/filepath"

	"gopkg.in/yaml.v3"
)

type Preferences struct {
	Remote string `yaml:"remote"`
}

type File struct {
	Preferences Preferences `yaml:"preferences"`
}

func Read() (*File, error) {
	usr, err := osUser.Current()
	if err != nil {
		return nil, err
	}
	gicoFolder := ensureFolder(usr.HomeDir)
	configPath := filepath.Join(gicoFolder, "config.yaml")
	var data []byte
	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		defaultString, err := initDefault(configPath)
		if err != nil {
			return nil, err
		}
		data = []byte(defaultString)
	} else {
		data, err = ioutil.ReadFile(configPath)
		if err != nil {
			return nil, err
		}
	}
	var file File
	err = yaml.Unmarshal(data, &file)
	if err != nil {
		return nil, err
	}
	return &file, nil

}
