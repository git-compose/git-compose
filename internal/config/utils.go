// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package config

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func initDefault(configPath string) (string, error) {
	defaultConfig := `
preferences:
  # choose between https and ssh, https is default
  remote: https
`
	err := ioutil.WriteFile(configPath, []byte(defaultConfig), 0644)
	if err != nil {
		return "", err
	}
	return defaultConfig, nil
}

func ensureFolder(home string) string {
	gicoFolder := filepath.Join(home, ".gico")
	if _, err := os.Stat(gicoFolder); os.IsNotExist(err) {
		log.Printf("Creating %s", gicoFolder)
		os.Mkdir(gicoFolder, 0744)
	}
	return gicoFolder
}
