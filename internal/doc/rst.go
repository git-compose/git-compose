// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package doc

import (
	"bytes"
	"text/template"

	"github.com/urfave/cli/v2"
)

var RstDocTemplate = `CLI
===

.. role:: bash(code)
   :language: bash

{{ .App.Name }}{{ if .App.Usage }} - {{ .App.Usage }}{{ end }}

SYNOPSIS
--------
{{ .App.Name }}
{{ if .App.UsageText }}
DESCRIPTION
^^^^^^^^^^^

{{ .App.UsageText }}
{{ end }}
**Usage**:

:bash:` + "`" + `{{ .App.Name }} {{ if .App.VisibleFlags }}[GLOBAL OPTIONS] {{ end }}command [COMMAND OPTIONS] [ARGUMENTS...]` + "`" + `

{{ if .App.VisibleFlags }}
GLOBAL OPTIONS
^^^^^^^^^^^^^^

{{ range $v := .App.VisibleFlags }}
{{ $v }}{{ end }}{{ end }}

{{ define "command" }}
{{ .App.Name }} {{range .Parents }}{{.Name}} {{ end }}{{ .Cmd.Name }}
-----------------------------------------

{{ .Cmd.Usage }}.{{ if .Cmd.UsageText }}

{{ .Cmd.UsageText }}{{ end }}
{{ if .Cmd.VisibleFlags}}
OPTIONS
^^^^^^^
{{ range $v := .Cmd.VisibleFlags }}{{ $v }}
{{ end }}{{ end }}{{ range $v := .Subcommands }}{{ template "command" $v }}{{ end }}{{ end }}

COMMANDS
--------

{{ range $v := .Commands }}{{ template "command" $v }}{{ end }}

`

type rstTemplateModel struct {
	App      *cli.App
	Commands []*rstTemplateCommandContext
}

type rstTemplateCommandContext struct {
	Cmd         *cli.Command
	App         *cli.App
	Subcommands []*rstTemplateCommandContext
	Parents     []*cli.Command
}

func ToRestructuredText(a *cli.App) (string, error) {
	var w bytes.Buffer
	t, err := template.New("cli").Parse(RstDocTemplate)

	if err != nil {
		return "", err
	}
	cmds := fillCommandsContext(a.VisibleCommands(), a, nil)
	err = t.ExecuteTemplate(&w, "cli", &rstTemplateModel{
		App:      a,
		Commands: cmds,
	})
	if err != nil {
		return "", err
	}
	return w.String(), nil
}

func fillCommandsContext(commands []*cli.Command, a *cli.App, parents []*cli.Command) []*rstTemplateCommandContext {
	cmds := make([]*rstTemplateCommandContext, len(commands))
	for i, cmd := range commands {
		newParents := append(parents, cmd)
		cmds[i] = &rstTemplateCommandContext{
			Cmd:         cmd,
			App:         a,
			Subcommands: fillCommandsContext(cmd.Subcommands, a, newParents),
			Parents:     parents,
		}
	}
	return cmds
}
