install:
	go install -ldflags "-X 'main.BuildVersion=$$(cat NEXTVERSION)'" cmd/gico/gico.go
	go install -ldflags "-X 'main.BuildVersion=$$(cat NEXTVERSION)'" cmd/git-compose/git-compose.go

gen-md:
	go run cmd/gico/gico.go doc md > gico.md

gen-man:
	go run cmd/gico/gico.go doc man > gico.man

gen-rst:
	go run cmd/gico/gico.go doc rst > gico.rst

convey-cmd: 
	( cd internal/cmd ; $${GOPATH}/bin/goconvey )

GOOGLEAPIS = "${HOME}/go/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.14.6/third_party/googleapis"
gen: 
	protoc -I ${GOOGLEAPIS} \
		   -I gico_api \
		   --go_out=plugins=grpc:gico_api --go_opt=paths=source_relative gico_api/gico.proto

srv-version:
	grpcurl -import-path "${GOOGLEAPIS}" \
	  -plaintext -import-path ./gico_api -proto gico.proto \
	  -d '{}' \
	  127.0.0.1:8080 \
	  gico.Gico/GetVersion

srv-branches:
	grpcurl -import-path "${GOOGLEAPIS}" \
	  -plaintext -import-path ./gico_api -proto gico.proto \
	  -d '{}' \
	  127.0.0.1:8080 \
	  gico.Gico/GetBranches

srv-parse:
	grpcurl -import-path "${GOOGLEAPIS}" \
	  -plaintext -import-path ./gico_api -proto gico.proto \
	  -d '{}' \
	  127.0.0.1:8080 \
	  gico.Gico/ParseFile

serve: gen install
	( cd .. ; gico serve )

test:
	go test ./internal/cmd