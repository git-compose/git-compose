% gico 8

# NAME

gico - helps you manage several Git repositories at once

# SYNOPSIS

gico

```
[--help|-h]
[--version|-v]
```

**Usage**:

```
gico [GLOBAL OPTIONS] command [COMMAND OPTIONS] [ARGUMENTS...]
```

# GLOBAL OPTIONS

**--help, -h**: show help

**--version, -v**: print the version


# COMMANDS

## up

does all necessary to bring your repos up to date

**--file, -f**="": location of git-compose.yaml file or - to take input stream

**--prefer-https**: if specified and alternatives available in 'remotes' - would choose HTTPS

**--prefer-ssh**: if specified and alternatives available in 'remotes' - would choose SSH

**--repo, -r**="": if specified - limits to only one repository

## init

initiate git-compose.yaml file, possibly from remote source

## push

Pushes all repositories

**--file, -f**="": location of git-compose.yaml file or - to take input stream

**--prefer-https**: if specified and alternatives available in 'remotes' - would choose HTTPS

**--prefer-ssh**: if specified and alternatives available in 'remotes' - would choose SSH

**--repo, -r**="": if specified - limits to only one repository

## exec

Executes command in all repositories

**--file, -f**="": location of git-compose.yaml file or - to take input stream

**--prefer-https**: if specified and alternatives available in 'remotes' - would choose HTTPS

**--prefer-ssh**: if specified and alternatives available in 'remotes' - would choose SSH

**--repo, -r**="": if specified - limits to only one repository

## clone

Clones all repositories to subfolders

**--file, -f**="": location of git-compose.yaml file or - to take input stream

**--prefer-https**: if specified and alternatives available in 'remotes' - would choose HTTPS

**--prefer-ssh**: if specified and alternatives available in 'remotes' - would choose SSH

**--repo, -r**="": if specified - limits to only one repository

## branch

Outputs list of all repos with current branches

**--file, -f**="": location of git-compose.yaml file or - to take input stream

**--prefer-https**: if specified and alternatives available in 'remotes' - would choose HTTPS

**--prefer-ssh**: if specified and alternatives available in 'remotes' - would choose SSH

## config

access user configuration

### view

outputs current configuration

### set

modifies current configuration

**--prefer-remote**="": set 'https' or 'ssh' to choose default preference for remote alternatives (default: https)

## completion

outputs script to autocomplete for this CLI

## version

prints current version

**--normal**: pass to receive only normal version: Major, Minor Patch

## serve

Starts server with GRPC API similar to this CLI

**--listen-port**="": choose port for server to listen to (default: 8080)

## parse

Parses git-compose.yaml into json and adds some additional info

**--file, -f**="": location of git-compose.yaml file or - to take input stream

**--prefer-https**: if specified and alternatives available in 'remotes' - would choose HTTPS

**--prefer-ssh**: if specified and alternatives available in 'remotes' - would choose SSH

## doc

outputs documentation in markdown ('md'), man or rst format

**--help, -h**: show help

## help, h

Shows a list of commands or help for one command
